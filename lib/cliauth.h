#ifndef cliauth_h
#define cliauth_h

#include <string>

class CliAuth
{
    private:
        const std::string _appId;
    public:
        CliAuth();
        std::string getAppId();
};

#endif
