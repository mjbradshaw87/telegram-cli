# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/michaelbradshaw/Developer/cpp/telegram-cli/cliauth.cpp" "/home/michaelbradshaw/Developer/cpp/telegram-cli/build/CMakeFiles/tele-cli.dir/cliauth.cpp.o"
  "/home/michaelbradshaw/Developer/cpp/telegram-cli/telegram-cli.cpp" "/home/michaelbradshaw/Developer/cpp/telegram-cli/build/CMakeFiles/tele-cli.dir/telegram-cli.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
